package adm.facultad;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	
	static Scanner sc = new Scanner(System.in);
	static String insNombre, carNombre, matNombre, profNombre, profApellido, estuNombre, estuApellido,  optMain="s", carrCod, matCod;
	static double sueldBasico;
	static int op = 0, err = 0, carrCodigo = 0, mateCodigo = 0, antiguedad = 0;
	static Carrera carrera = null;
	static Materia materia = null;
	static Profesor profesor = null;
	static Estudiante estudiante = null;

	public static void main(String[] args) {
		
		
		
		System.out.println("Ingrese el nombre de la instituci�n");
		insNombre = sc.nextLine();

		Facultad facultad = new Facultad(insNombre);
		
		
		do {
			System.out.println("Menu:\n"
					+ "1)Crear una carrera.\n"
					+ "2)Crear una materia.\n"
					+ "3)Crear un profesor.\n"
					+ "4)Cambiar titular.\n"
					+ "5)Crear un alumno.\n"
					+ "6)Ver inf. de Facultad\n"
					
					+ "0)Salir.");
			
			//Eleccion del menu
			do {
				try {
					optMain = sc.next();
					op = Integer.parseInt(optMain);
					optMain="s";
					err=0;
				}catch(Exception e) {
					System.out.println("ERROR! Ingrese un valor n�merico y que el mismo se encuentre contemplado en el men�.");
					System.out.println("INTENTELO DE NUEVO");
					err=-1;
				}

			}while(err==-1);
			
			switch(op) {
				case 1:
					System.out.println("Ingrese el nombre de la carrera.");
					carNombre = getScanner(sc).nextLine();
					carrera = new Carrera(carNombre);
					facultad.agregarCarrera(carrera);
				break;
				case 2:
					crearMateria(facultad);
				break;
				case 3:
					agregarCambiarTitular(facultad,3);
				break;
				case 4:
					agregarCambiarTitular(facultad,4);
				break;
				case 5:
					crearEstudiante(facultad);
				break;
				case 6:
					System.out.println("Informacion de la Facultad:\n" + facultad.toString());
				break;
				case 0:
					System.out.println("Cerrando aplicacion...");
				break;
				default:
					System.out.println("Opcion invalida!.");
			}
			
			if(op != 0 || err == -1) {
				do {
					System.out.println("Desea realizar otra operacion? (s/n)");
					optMain = sc.next();
					
				}while(!optMain.equalsIgnoreCase("s") && !optMain.equalsIgnoreCase("n"));
						
			}
			
		}while( op != 0 && !optMain.equalsIgnoreCase("n"));
	}
	
	
	private static Scanner getScanner(Scanner sc) {
		sc = null;
		return new Scanner(System.in);
	}
	
	private static  void crearMateria(Facultad facultad) {
		System.out.println("-----------------------------------------------------");
		for (Carrera carr : facultad.getColeccionCarreras()) {
			System.out.println(carr.getCodigoCarrera() + " | " + carr.getNombre());
		}
		
		//Eleccion del codigo de la carrera
		do {
			try {
				System.out.println("Indique el codigo de la carrera a la que pertenece.");
				carrCod = getScanner(sc).nextLine();
				carrCodigo = Integer.parseInt(carrCod);
				err=buscarCarrera(facultad, carrCodigo);
			}catch(Exception e) {
				
				err=-1;
			}
			
			if(err == -1) {
				System.out.println("Recuerde que los caracteres validos son n�mericos y el codigo ingresado debe existir.");
				do {
					System.out.println("Desea Volver? (s/n)");
					optMain = getScanner(sc).nextLine();	
				}while(!optMain.equalsIgnoreCase("s") && !optMain.equalsIgnoreCase("n"));
				
				if(optMain.equalsIgnoreCase("s")) {
					return;
				}
			}
			
		}while(err==-1);
		
		for (Carrera carr : facultad.getColeccionCarreras()) {
			if(carr.getCodigoCarrera() == carrCodigo) {
				carrera = carr;
			}
		}
		
		
		System.out.println("Ingrese el nombre de la materia.");
		matNombre = getScanner(sc).nextLine();

		materia = new Materia(matNombre);
		carrera.agregarMateria(materia);
		
	}
	
	private static int buscarCarrera(Facultad facultad, int cod) {
		if(facultad.getColeccionCarreras().size() > 0) {
			for (Carrera carr : facultad.getColeccionCarreras()) {
				if(carr.getCodigoCarrera() == cod)
					return carr.getCodigoCarrera();
			}
		}
		return -1;
	}
	
	private static  void agregarCambiarTitular(Facultad facultad, int opt) {
		System.out.println("-----------------------------------------------------");
		for (Carrera carr : facultad.getColeccionCarreras()) {
			System.out.println(carr.getCodigoCarrera() + " | " + carr.getNombre());
		}
		
		do {
			try {
				System.out.println("Indique el codigo de la carrera a la que pertenece.");
				carrCod = getScanner(sc).nextLine();
				carrCodigo = Integer.parseInt(carrCod);
				err=buscarCarrera(facultad, carrCodigo);
			}catch(Exception e) {
				
				err=-1;
			}
			
			if(err == -1) {
				System.out.println("Recuerde que los caracteres validos son n�mericos y el codigo ingresado debe existir.");
				do {
					System.out.println("Desea Volver? (s/n)");
					optMain = getScanner(sc).nextLine();	
				}while(!optMain.equalsIgnoreCase("s") && !optMain.equalsIgnoreCase("n"));
				
				if(optMain.equalsIgnoreCase("s")) {
					return;
				}
			}
			
		}while(err==-1);
		
		
		for (Carrera carr : facultad.getColeccionCarreras()) {
			if(carr.getCodigoCarrera() == carrCodigo) {
				carrera = carr;
			}
		}
		
		System.out.println("Lista de materias.!");
		
		for (Materia mate : carrera.getColeccionMaterias()) {
			System.out.println(mate.getCodigoMateria() + " | " + mate.getNombre());
		}
		
		System.out.println("Indique el codigo de la materia a la que pertenece.");
		//Eleccion del codigo de la materia
		do {
			try {

				matCod = getScanner(sc).nextLine();
				mateCodigo = Integer.parseInt(matCod);
				err=buscarCarrera(facultad, mateCodigo);
			}catch(Exception e) {
				
				err=-1;
			}
		
			if(err == -1) {
				System.out.println("Recuerde que los caracteres validos son n�mericos y el codigo ingresado debe existir.");
				do {
					System.out.println("Desea Volver? (s/n)");
					optMain = getScanner(sc).nextLine();	
				}while(!optMain.equalsIgnoreCase("s") && !optMain.equalsIgnoreCase("n"));
				
				if(optMain.equalsIgnoreCase("s")) {
					return;
				}
			}
			
			
		}while(err==-1);
		
		
		for (Materia mate : carrera.getColeccionMaterias()) {
			if(mate.getCodigoMateria() == mateCodigo) {
				materia = mate;
			}
		}
		
		System.out.println("Ingrese el nombre de profesor.");
		profNombre = getScanner(sc).nextLine();
		System.out.println("Ingrese el apellido del profesor.");
		profApellido = getScanner(sc).nextLine();
		System.out.println("Ingrese el sueldo basico del profesor.");
		sueldBasico = sc.nextDouble();
		System.out.println("Ingrese la antiguedad.");
		antiguedad = sc.nextInt();
		
		profesor = new Profesor(profNombre, profApellido, sueldBasico, antiguedad);
		if(opt == 3) {
			materia.setTitular(profesor);	
		}else {
			materia.modificarTitular(profesor);
		}
		
	}
	
	private static  void crearEstudiante(Facultad facultad) {
		String resp = null;
		List<Integer> codigoMaterias = new ArrayList<Integer>();
		List<Materia> listaMaterias = new ArrayList<Materia>();
		
		System.out.println("-----------------------------------------------------");
		for (Carrera carr : facultad.getColeccionCarreras()) {
			System.out.println(carr.getCodigoCarrera() + " | " + carr.getNombre());
		}
		System.out.println("Indique el codigo de la carrera a la que pertenece.");
		//Eleccion del codigo de la carrera
		do {
			try {
				carrCod = getScanner(sc).nextLine();
				carrCodigo = Integer.parseInt(carrCod);
				err=buscarCarrera(facultad, carrCodigo);

			}catch(Exception e) {
				
				err=-1;
			}
			
			if(err == -1) {
				System.out.println("Recuerde que los caracteres validos son n�mericos y el codigo ingresado debe existir.");
				do {
					System.out.println("Desea Volver? (s/n)");
					optMain = getScanner(sc).nextLine();	
				}while(!optMain.equalsIgnoreCase("s") && !optMain.equalsIgnoreCase("n"));
				
				if(optMain.equalsIgnoreCase("s")) {
					return;
				}
			}
			
		}while(err==-1);
		
		for (Carrera carr : facultad.getColeccionCarreras()) {
			if(carr.getCodigoCarrera() == carrCodigo) {
				carrera = carr;
			}
		}
		
		System.out.println("Lista de materias.!");
		
		for (Materia mate : carrera.getColeccionMaterias()) {
			System.out.println(mate.getCodigoMateria() + " | " + mate.getNombre());
		}
		
		
		//Eleccion del codigo de la materia
		do {
			try {
				System.out.println("Indique el codigo de la materia a la quiere inscribirse.");
				//mateCodigo = sc.nextInt();
				matCod = getScanner(sc).nextLine();
				mateCodigo = Integer.parseInt(matCod);
				
				System.out.println("Desea seleccionar otra?(s/n)");
				resp = getScanner(sc).nextLine();
				err=buscarCarrera(facultad, mateCodigo);
				//err=0;				
			}catch(Exception e) {
				System.out.println("Recuerde que los caracteres validos son numericos y el codigo ingresado debe existir.");
				err=-1;
			}
			
		}while(err==-1 || resp.equalsIgnoreCase("s"));
		
		for (Integer codi : codigoMaterias) {
			for (Materia mate : carrera.getColeccionMaterias()) {
				if(mate.getCodigoMateria() == codi) {
					listaMaterias.add(mate);
				}
			}
		}
		
		
		System.out.println("Ingrese el nombre del estudiante.");
		estuNombre = getScanner(sc).nextLine();
		System.out.println("Ingrese el apellido del estudiante.");
		estuApellido = getScanner(sc).nextLine();
		
		estudiante = new Estudiante();
		estudiante.nombre = estuNombre;
		estudiante.apellido = estuApellido;
		
		for (Materia materia : listaMaterias) {
			materia.agregarEstudiante(estudiante);
		}
	}

}
