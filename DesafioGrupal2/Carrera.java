package adm.facultad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Carrera implements Informacion {

	private static int codigo = 0;
	private int codigoCarrera;
	private String nombre;
	private ArrayList<Materia> coleccionMaterias;
	
	
	public Carrera(String nombre, ArrayList<Materia> coleccionMaterias) {
		super();
		this.nombre = nombre;
		this.coleccionMaterias = coleccionMaterias;
		this.codigoCarrera = ++codigo;
	}

	public Carrera() {
		super();
		this.codigoCarrera = ++codigo;
	}

	public Carrera(String nombre) {
		super();
		this.nombre = nombre;
		this.codigoCarrera = ++codigo;
	}

	public int getCodigoCarrera() {
		return codigoCarrera;
	}

	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setColeccionMaterias(ArrayList<Materia> coleccionMaterias) {
		this.coleccionMaterias = coleccionMaterias;
	}
	

	public ArrayList<Materia> getColeccionMaterias() {
		if(this.coleccionMaterias == null)
			this.coleccionMaterias = new ArrayList<>();
		return coleccionMaterias;
	}


	public void agregarMateria(Materia materias) {
		if(this.coleccionMaterias == null) {
			this.coleccionMaterias = new ArrayList<>();
		}
		
		this.coleccionMaterias.add(materias);
	}
	
	public void eliminarMateria(String nombreMateria) {
		for (int i = 0; i < this.coleccionMaterias.size(); i++) {
			if(this.coleccionMaterias.get(i).getNombre().equals(nombreMateria)) {
				this.coleccionMaterias.remove(i);
			}
		}
	}

	public void encontrarMateria (String nombre) {
		try (Scanner sc = new Scanner(System.in)) {
			String rta = "";
			for (int i = 0; i < this.coleccionMaterias.size(); i++) {
				if(this.coleccionMaterias.get(i).getNombre().equals(nombre)) {
					System.out.println("Desea borrar esta materia?(s/N)");
					rta = sc.next();
				}
			}
			
			if(rta.equalsIgnoreCase("s")) {
				eliminarMateria(nombre);
			}
		}
	}

	@Override
	public int verCantidad() {
		return this.coleccionMaterias.size();
	}

	@Override
	public String listarContenidos() {
		Collections.sort(this.coleccionMaterias, new Comparator<Materia>() {

			@Override
			public int compare(Materia o1, Materia o2) {
				return o1.getNombre().compareTo(o2.getNombre());
			}
			
		});
		
		StringBuilder sb = new StringBuilder();
		for (Materia materia : coleccionMaterias) {
			sb.append(materia.getNombre() + "\n");
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return "Carrera: " + nombre + 
				"\nMaterias:\n" + listarContenidos() + 
				"\nTotal de materias: " + verCantidad() +
				"\n----------------------------------------------------";
	}
	
}
