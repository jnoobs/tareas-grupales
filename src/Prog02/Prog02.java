package desafios;

class Prog02 {
	

	    public static void main(String[] args) {

	        float matriz[][] = new float[3][3];
	        for(int x=0; x<3; x++) {
	            for(int y=0; y<3; y++) {
	                matriz[x][y] = (float) Math.random()*10;
	            }
	        }
	        mostrar_matriz(matriz);

	    }

	    public static void mostrar_matriz(float matriz[][]) {

	        for(int x=0; x<3; x++) {
	            for(int y=0; y<3; y++) {
	                System.out.print("[" + matriz[x][y] + "]" + " ");
	            }
	            System.out.println();
	        }
	        System.out.println();

	    }

	}

