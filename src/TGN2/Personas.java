package TGN2;

public abstract class Personas {

	protected String nombre;
	protected int edad;
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	public abstract String getTipo();

	@Override
	public String toString() {
		return "Personas [nombre=" + nombre + ", edad=" + edad + "]";
	}
	
}
