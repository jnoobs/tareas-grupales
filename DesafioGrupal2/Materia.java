package adm.facultad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Materia implements Informacion{

	private static int codigo = 0;
	private int codigoMateria;
	private String nombre;
	private Profesor titular;
	private ArrayList<Estudiante> coleccionEstudiantes;
	
	
	public Materia() {
		super();
		this.codigoMateria = ++codigo;
	}
	
	public Materia(String nombre, Profesor titular, ArrayList<Estudiante> coleccionEstudiantes) {
		super();
		this.nombre = nombre;
		this.titular = titular;
		this.coleccionEstudiantes = coleccionEstudiantes;
		this.codigoMateria = ++codigo;
	}

	public Materia(String nombre) {
		this.nombre = nombre;
		this.codigoMateria = ++codigo;
	}
	
	

	public int getCodigoMateria() {
		return codigoMateria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Profesor getTitular() {
		return titular;
	}

	public void setTitular(Profesor titular) {
		this.titular = titular;
	}

	public ArrayList<Estudiante> getColeccionEstudiantes() {
		return coleccionEstudiantes;
	}

	public void setColeccionEstudiantes(ArrayList<Estudiante> coleccionEstudiantes) {
		this.coleccionEstudiantes = coleccionEstudiantes;
	}
	
	
	public void agregarEstudiante(Estudiante estudiantes) {
		if(this.coleccionEstudiantes == null) {
			this.coleccionEstudiantes = new ArrayList<>();
		}
		
		this.coleccionEstudiantes.add(estudiantes);
	}
	
	public void eliminarEstudiante (String nombre) {
		for (int k = 0; k < this.coleccionEstudiantes.size(); k++) {
			if(this.coleccionEstudiantes.get(k).nombre.equals(nombre)) {
				this.coleccionEstudiantes.remove(k);
			}
		}
	}
	
	public void modificarTitular (Profesor profesor) {
		this.titular = profesor;
	}

	@Override
	public int verCantidad() {
		return this.coleccionEstudiantes.size();
	}

	@Override
	public String listarContenidos() {
		Collections.sort(this.coleccionEstudiantes, new Comparator<Estudiante>() {

			@Override
			public int compare(Estudiante o1, Estudiante o2) {
				return o1.apellido.compareTo(o2.apellido);
			}
			
		});
		
		StringBuilder sb = new StringBuilder();
		for (Estudiante estudiante : coleccionEstudiantes) {
			sb.append(estudiante.apellido + estudiante.nombre + "\n");
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return "\nMateria: " + nombre + 
				"\nProfesor de la materia: " + titular + 
				"\nLista de estudiantes:\n" + listarContenidos() +
				"\nTotal de estidiantes que cursan la materia: " + verCantidad() +
				"\n----------------------------------------------------";
	}
	
	
	
}
