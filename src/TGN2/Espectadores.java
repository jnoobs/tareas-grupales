package TGN2;

public class Espectadores extends Personas {

	private String fila;
	private int silla;
	
	public Espectadores(String nombre, int edad, String fila, int silla) {
		this.nombre = nombre;
		this.edad = edad;
		this.fila = fila;
		this.silla = silla;
	}
	
	public String getFila() {
		return fila;
	}

	public void setFila(String fila) {
		this.fila = fila;
	}
	
	public int getSilla() {
		return silla;
	}

	public void setSilla(int silla) {
		this.silla = silla;
	}

	@Override
	public String getTipo() {
		return "Espectadores";
	}

	@Override
	public String toString() {
		return "Espectadores [fila=" + fila + ", silla=" + silla + ", nombre=" + nombre + ", edad=" + edad
				+ ", tipo=" + getTipo() + "]";
	}

	
}
