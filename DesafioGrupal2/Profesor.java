package adm.facultad;

public class Profesor extends Persona {

	private double basico;
	private int antiguedad;
	
	
	public Profesor() {
		super();
		this.legajo = ++codigo;
	}
	
	public Profesor(String nombre, String apellido, double basico, int antiguedad) {
		super.nombre = nombre;
		super.apellido = apellido;
		this.basico = basico;
		this.antiguedad = antiguedad;
		this.legajo = ++codigo;
	}
	
	
	
	public double getBasico() {
		return basico;
	}

	public void setBasico(double basico) {
		this.basico = basico;
	}

	public int getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}

	public Profesor(double basico, int antiguiedad) {
		super();
		this.basico = basico;
		this.antiguedad = antiguiedad;
	}

	public double calcularSueldo() {
		
		double porcentajeASumar, adicional;
		porcentajeASumar = antiguedad * 0.1;
		adicional = (basico * porcentajeASumar)/100;
		
		return basico + adicional;
	}

	@Override
	public String toString() {
		return "Informacion del Porofesor: \n "
				+ "Apellido y Nombre: " + apellido + nombre +
				"\nSueldo basico: $" + basico + ", antiguedad de " + antiguedad +" a�os." +
				"\nSueldo con el aumento correspondiente: $" + calcularSueldo() + 
				"\nLegajo: " + legajo;
	}


	
}