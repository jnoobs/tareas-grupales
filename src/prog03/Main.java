package prog03;


import java.io.Console;
import java.util.Scanner;


public class Main {

	public static void main (String [] args) {
		
		Scanner sc = new Scanner (System.in);
		
		int numPersona;
		
		System.out.println("Ingrese la cantidad de personas a cargar:");
		numPersona = sc.nextInt();
		
		String persona[][] = new String[numPersona][3];

		for(int i=0; i < numPersona; i++) {
			System.out.println("Persona " + (i+1));
			for(int j=0; j < 3; j++) {
				
				if(j == 0){
					System.out.println("Ingrese nombre:");
					persona[i][j] = sc.next(); 
				}else if(j == 1) {
					System.out.println("Ingrese dni:");
					persona[i][j] = sc.next(); 
				}else {
					System.out.println("Ingrese edad:");
					persona[i][j] = sc.next(); 
				}
				
			}
		}
		
		persona = ordenarPorBurbuja(persona, numPersona);
		
		System.out.println("Lista de personas:"); 
		       
		
        for (int i = 0; i < 3; i++) { 
            for (int j = 0; j < numPersona; j++) {
            	System.out.printf("%-10s",persona[j][i]);
            }
            System.out.println();
            
        }
        
       
	}
	
	private static String[][] ordenarPorBurbuja(String [][] matriz, int numPersona){
		
		
		for(int i=0; i < numPersona-1; i++) {
			
			String postuladoNombre = matriz[i][0];
			String postuladoDni = matriz[i][1];
			String postuladoEdad = matriz[i][2];
			
			for(int j=i+1; j < numPersona; j++) {
				
				if(matriz[i][0].compareTo(matriz[j][0]) > 0) {
					
					matriz[i][0] = matriz[j][0];
					matriz[i][1] = matriz[j][1];
					matriz[i][2] = matriz[j][2];
					
					matriz[j][0] = postuladoNombre;
					matriz[j][1] = postuladoDni;
					matriz[j][2] = postuladoEdad;
				}
				
			}
		}
		
		return matriz;
	}
}
