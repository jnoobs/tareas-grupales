package Desafio13;

import java.util.Scanner;

class Prog05 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		Scanner scan = new Scanner(System.in);
        float matriz[][] = new float[3][3];
        for(int x=0; x<3; x++) {
            for(int y=0; y<3; y++) {
                System.out.println("Ingresar valor flotante elemento " + x + " " + y);
                matriz[x][y] = scan.nextFloat();
            }
        }
        mostrar_matriz(matriz);
        ordenar(matriz);

    }

    public static void mostrar_matriz(float matriz[][]) {

        for(int x=0; x<3; x++) {
            for(int y=0; y<3; y++) {
                System.out.print("[" + matriz[x][y] + "]" + " ");
            }
            System.out.println();
        }
        System.out.println();

    }

    public static void ordenar(float matriz[][]) {

        float temp = 0;
        int x, y, j, i = 0;
        for(x=0; x<3; x++) {
            for(y=0; y<3; y++) {
                for(j=0; j<3; j++) {
                    for(i=0; i<3; i++) {
                        if(matriz[x][y] < matriz[j][i]) {
                            temp = matriz[j][i];
                            matriz[j][i] = matriz[x][y];
                            matriz[x][y] = temp;
                        }
                    }
                }
            }
        }
        mostrar_matriz(matriz);

    }
		
		
		
		
	}


