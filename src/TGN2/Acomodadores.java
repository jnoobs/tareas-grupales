package TGN2;

public class Acomodadores extends Empleados implements ParaAcomodadores {

	private Salas sala;
	
	public Acomodadores (String nombre, int edad) {
		super(nombre,edad);
	}
	
	public Salas getSala() {
		return this.sala;
	}
	
	@Override
	public String getTipo() {
		return "Acomodador";
	}
	
	public void setSala(Salas sala) {
		this.sala = sala;
	}

	@Override
	public String toString() {
		return "Acomodador de la " + sala.getNombre() + ", Sueldo $" + sueldo + ", Nombre" + nombre + ", tiene " + edad
				+"a�os." + ", Puesto de " + getTipo();
	}

	
	
	
}