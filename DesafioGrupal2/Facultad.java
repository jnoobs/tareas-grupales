package Adm.facultad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Facultad implements Informacion {
	
	private String nombre;
	private  ArrayList<Carrera> coleccionCarreras;
	
	public Facultad(String nombre, ArrayList<Carrera> coleccionCarreras) {
		super();
		this.nombre = nombre;
		this.coleccionCarreras = coleccionCarreras;
	}

	public Facultad(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	public Facultad() {
		super();

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Carrera> getColeccionCarreras() {
		if(this.coleccionCarreras == null)
			this.coleccionCarreras = new ArrayList<>();
			
		return this.coleccionCarreras;
	}

	public void setColeccionCarreras(ArrayList<Carrera> coleccionCarreras) {
		this.coleccionCarreras = coleccionCarreras;
	}
	
	public void agregarCarrera (Carrera careras) {
		if(this.coleccionCarreras == null) {
			this.coleccionCarreras = new ArrayList<>();
		}
		this.coleccionCarreras.add(careras);
	}
	
	public void eliminarCarrera (String nombre) {
		for (int i = 0; i < this.coleccionCarreras.size(); i++) {
			if( this.coleccionCarreras.get(i)
					.getNombre().equalsIgnoreCase(nombre) ) {
				this.coleccionCarreras.remove(i);
			}
		}
	}
	
	public void eliminarEstudiante (Estudiante estudiante) {
		for (int i = 0; i < this.coleccionCarreras.size(); i++) {
			List<Materia> materias = this.coleccionCarreras.get(0).getColeccionMaterias();
			
			for (int j = 0; j < materias.size(); j++) {
				List<Estudiante> estudiantes = materias.get(0).getColeccionEstudiantes();
				
				
				for (int k = 0; k < estudiantes.size(); k++) {
					if(estudiantes.get(k).equals(estudiante)) {
						estudiantes.remove(k);
					}
				}
				
			}
		}
	}

	@Override
	public int verCantidad() {
		return this.coleccionCarreras.size();
	}

	@Override
	public String listarContenidos() {
		Collections.sort(this.coleccionCarreras, new Comparator<Carrera>() {

			@Override
			public int compare(Carrera o1, Carrera o2) {
				return o1.getNombre().compareTo(o2.getNombre());
			}
			
		});
		
		StringBuilder sb = new StringBuilder();
		for (Carrera carrera : coleccionCarreras) {
			sb.append(carrera.getNombre() + "\n");
		}
		return sb.toString();
	}


	@Override
	public String toString() {
		return "Facultad: " + nombre + 
				"\nCarreras:\n" + listarContenidos() +
				"Total de carreras: "+ verCantidad() +
				"\n----------------------------------------------------";
	}

	


	
}

