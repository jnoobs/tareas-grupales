package TGN2;



	import java.util.Arrays;

	public class Salas {

		private int capacidad;
		private String pelicula;
		private String nombre;
		private Espectadores[] espectadores;
		
		public Salas (int capacidad, String nombre, String pelicula) {
			this.capacidad = capacidad;
			this.nombre = nombre;
			this.pelicula = pelicula; 
		}
		
		public int getCapacidad() {
			return capacidad;
		}

		public void setCapacidad(int capacidad) {
			this.capacidad = capacidad;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getPelicula() {
			return pelicula;
		}



		public void setPelicula(String pelicula) {
			this.pelicula = pelicula;
		}
		
		public void setEspectadores(Espectadores[] espectadores) {
			if(espectadores.length <= this.capacidad)
				this.espectadores = espectadores;
			else
				System.out.println("No existe esa cantidad de ascientos.");
		}
		
		
		
		public String getEspectadores() {
			StringBuilder builder = new StringBuilder();
			Espectadores espect = null;
			for (int i = 0; i < espectadores.length; i++) {
				if(espectadores[i] != null) {
					espect = espectadores[i];
					builder.append(espectadores[i].nombre + "\n");
				}
			}
			
			if(espect == null) {
				builder.append("SIN ESPECTADORES CARGADOS\n");
			}
			return builder.toString();
		}

		@Override
		public String toString() {
			return "Capacidad de sala: " + capacidad + ", pelicula: " + pelicula + ", nombre=" + nombre + ", espectadores="
					+ Arrays.toString(espectadores) ;
		}
		
	
}
