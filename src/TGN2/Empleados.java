package TGN2;

public class Empleados extends Personas{

	protected double sueldo;
	
	public Empleados (String nombre, int edad) {
		super.nombre = nombre;
		super.edad = edad;
	}
	
	public void setSueldo (double sueldo) {
		this.sueldo = sueldo;
	}
	
	public String getTipo() {
		return "Empleado";
	}

	@Override
	public String toString() {
		return "Empleado con sueldo de $" + sueldo + ", Nombre: " + nombre + ", edad: " + edad + ", puesto: " + getTipo();
	}

	

}
