package TGN2;

import java.util.Scanner;

public class Cine {

	public static void main(String[] args) throws Exception  {

		Scanner sc = new Scanner(System.in);

		Espectadores [] espectador = new Espectadores[3];
		
		for (int i = 0; i < espectador.length; i++) {
			try {
				System.out.println("Ingrese el nombre del " + (i+1) + " espectador:");
				String nombre = cleanScanner(sc).nextLine();
				System.out.println("Ingrese la edad de " + nombre);
				int edad = cleanScanner(sc).nextInt();
				System.out.println("Ingrese la fila: ");
				String fila = cleanScanner(sc).next();
				
				if(fila.length() > 1 || fila.matches(".*\\d.*")) {
					throw new Exception("Recuerde que la sala debe ser una LETRA");
				}
				
				System.out.println("Ingrese la silla: ");
				int silla = cleanScanner(sc).nextInt();
				
				espectador[i] = new Espectadores(nombre, edad, fila, silla);
			}catch (Exception e) {
				System.out.println("Error en el ingreso de datos");
				throw e;
			}
		}
		
		System.out.println(" ");
		
		Salas s1  = new Salas(3, "SALA 01", "Jocker");
		s1.setEspectadores(espectador);
		
		System.out.println("Lista de espectadores: ");
		System.out.println(s1.getEspectadores());
		System.out.println("---------------------------------------------");
		
		
		Acomodadores acom1 = new Acomodadores("Pedro", 35);
		acom1.setSala(s1);
		acom1.setSueldo(42000);
		
		System.out.println(acom1.toString());
		System.out.println("---------------------------------------------");
		
		Empleados emp1 = new Empleados("Juan", 41);
		System.out.println(emp1.toString());
		System.out.println("---------------------------------------------");
		
	}
	
	private static Scanner cleanScanner(Scanner sc) {
		sc = null;
		return new Scanner(System.in);
	}

}
